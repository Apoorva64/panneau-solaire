import pickle

import pandas as pd

original_dataframe = pd.read_csv('out/best_tilt_azimuth_TF/optimized_tf.csv')

dataframe = original_dataframe.T
dataframe = dataframe.iloc[1:, :]

lat_lon_coordinates_filehandler = open("src/graphs/coordinates.pickle", "rb")
lat_lon_coordinates = pickle.load(lat_lon_coordinates_filehandler)

data_in_list = []
tilt = dataframe[0]
azimuth = dataframe[1]
transposition_factor = dataframe[2]

for i in range(len(dataframe[0])):
    data_in_list.append(
        (round(lat_lon_coordinates[i][0], 2), round(lat_lon_coordinates[i][1], 2), tilt[i], azimuth[i],
         transposition_factor[i]))
output_dataframe = pd.DataFrame(data_in_list, columns=['latitude', 'longitude', 'optimal tilt', 'optimal azimuth',
                                                       'transposition factor'])

output_dataframe.to_excel('transposition_factor_reformatted.xlsx')
