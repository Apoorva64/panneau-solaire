# from time import sleep

# from netCDF4 import Dataset
#
# import netCDF4
import pandas as pd

import xarray as xr
import os

# import tqdm

SATELLITE_DATA_DIRECTORIES = {
    'RAYT_HORAIRE_GLOBAL_2019': '../satellite_Data/data/RAYT_HORAIRE_GLOBAL_2019/',
    'RAYT_HORAIRE_DIRECT_2019': '../satellite_Data/data/RAYT_HORAIRE_DIRECT_2019/',
}


def read_satellite_data_rayt_horaire_global_2019(directory: str) -> pd.DataFrame:
    """
    Reads the rayt_horaire_global_2019 dataset in netCDF4 format and loads it into a pandas dataframe
    :param directory: directory of the satellite dataset
    :return:
    :rtype: pd.DataFrame
    """
    df_list = []
    print(f"reading dataset {directory}")
    for file in os.listdir(directory):
        nc = xr.open_dataset(directory + file)
        df = nc.SIS.to_dataframe()
        df_list.append(df)
    full_dataframe = pd.concat(df_list)
    print(f"loading complete {directory}")
    return full_dataframe


def read_satellite_data_rayt_horaire_direct_2019(directory: str) -> pd.DataFrame:
    """
    Reads the rayt_horaire_direct_2019 dataset in netCDF4 format and loads it into a pandas dataframe
    :param directory: directory of the satellite dataset
    :return:
    :rtype: pd.DataFrame
    """
    df_list = []
    print(f"reading dataset {directory}")
    for file in os.listdir(directory):
        nc = xr.open_dataset(directory + file)
        df = nc.SID.to_dataframe()
        df_list.append(df)
    full_dataframe = pd.concat(df_list)
    print(f"loading complete {directory}")
    return full_dataframe


def generate_pickled_satellite_data_rayt_horaire_global_2019() -> None:
    """
    Reads the RAYT_HORAIRE_GLOBAL_2019 dataset and converts it into a more usable format then pickles it
    """
    name = 'RAYT_HORAIRE_GLOBAL_2019'
    print('creating pickle for ' + name)
    data = read_satellite_data_rayt_horaire_global_2019(SATELLITE_DATA_DIRECTORIES[name])
    listx = []
    listy = []
    listz = []
    listtime = []
    for item in data.iloc(0):
        listtime.append(item.name[0])
        listx.append(item.name[1])
        listy.append(item.name[2])
        listz.append(item[0])
    out_data = pd.DataFrame(data=[listtime, listx, listy, listz], index=["Time", "lat", "lon", "SIS"]).T
    out_data.to_pickle(f'../out/refactored_satellite_data/{name}.pickle')


def generate_pickled_satellite_data_rayt_horaire_direct_2019() -> None:
    """
    Reads the RAYT_HORAIRE_DIRECT_2019 dataset and converts it into a more usable format then pickles it
    """
    name = 'RAYT_HORAIRE_DIRECT_2019'
    print('creating pickle for ' + name)
    data = read_satellite_data_rayt_horaire_direct_2019(SATELLITE_DATA_DIRECTORIES[name])
    listx = []
    listy = []
    listz = []
    listtime = []
    for item in data.iloc(0):
        listtime.append(item.name[0])
        listx.append(item.name[1])
        listy.append(item.name[2])
        listz.append(item[0])
    out_data = pd.DataFrame(data=[listtime, listx, listy, listz], index=["Time", "lat", "lon", "SID"]).T
    out_data.to_pickle(f'../out/refactored_satellite_data/{name}.pickle')


def generate_pickled_satellite_data_rayt_horaire_global_direct_2019():
    sis_dataframe = pd.read_pickle(r'../out/refactored_satellite_data/RAYT_HORAIRE_GLOBAL_2019.pickle')
    sid_dataframe = pd.read_pickle(r'../out/refactored_satellite_data/RAYT_HORAIRE_DIRECT_2019.pickle')
    full_dataframe = pd.merge(sis_dataframe, sid_dataframe)
    full_dataframe.to_pickle(r'../out/refactored_satellite_data/RAYT_HORAIRE_DIRECT_GLOBAL_2019.pickle')


def read_satellite_height_data(filepath):
    """
    Reads the height_data dataset in netCDF4 format and loads it into a pandas dataframe
    :param filepath: filepath of the satellite dataset
    :return:
    :rtype: pd.DataFrame
    """
    df_list = []
    print(f"reading dataset {filepath}")
    nc = xr.open_dataset(filepath)
    full_dataframe = pd.concat(df_list)
    print(f"loading complete {filepath}")
    return full_dataframe
