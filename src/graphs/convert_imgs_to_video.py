import os
import tqdm
from cv2.cv2 import imread, VideoWriter, VideoWriter_fourcc

img_array = []
IMG_PATH = r'../../out/graphs/RAYT_HORAIRE_DIRECT_GLOBAL_2019\\'
for filename in tqdm.tqdm(os.listdir(IMG_PATH)):
    img = imread(IMG_PATH + filename)
    # print(IMG_PATH+filename)
    height, width, layers = img.shape
    size = (width, height)
    img_array.append(img)

out = VideoWriter('../../out/SIS_SID_graph.avi', VideoWriter_fourcc(*'DIVX'), 10, size)

for i in range(len(img_array)):
    out.write(img_array[i])
out.release()
