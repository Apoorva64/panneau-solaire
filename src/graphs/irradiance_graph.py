import pickle

import pandas as pd
from matplotlib import pyplot as plt
import pytz
from pvlib import irradiance, location, tracking
import numpy as np

DNI = pd.read_pickle('../../out/refactored_satellite_data/RAYT_HORAIRE_DIRECT_2019.pickle')
GHI = dataframe = pd.read_pickle('../../out/refactored_satellite_data/RAYT_HORAIRE_GLOBAL_2019.pickle')
latitude_coordinates = pd.unique(DNI['lat'])
longitude_coordinates = pd.unique(DNI['lon'])

fig = plt.figure()
ax = plt.axes(projection='3d')
x_axis = []
y_axis = []
z_axis = []
coordinates = []
for latitude in latitude_coordinates:
    for longitude in longitude_coordinates:
        # For this example, we will be using Golden, Colorado
        tz = pytz.timezone("Etc/GMT+4")
        lat, lon = latitude, longitude
        coordinates.append((lat, lon))
        #
        # # Create location object to store lat, lon, timezone
        site_location = location.Location(lat, lon, tz=tz)

        # Calculate clear-sky GHI and transpose to plane of array
        # Define a function so that we can re-use the sequence of operations with
        # different locations

        dni_select1 = DNI[DNI['lat'] == latitude]
        dni_select2 = dni_select1[dni_select1['lon'] == longitude]
        dni_series = pd.Series(dni_select2['Time'].values, index=dni_select2['SID'])
        dni_series = pd.Series(dni_series.index.values, index=dni_series, name='dni')
        dni_series = dni_series.tz_localize(pytz.utc)

        ghi_select1 = GHI[GHI['lat'] == latitude]
        ghi_select2 = ghi_select1[ghi_select1['lon'] == longitude]
        ghi_series = pd.Series(ghi_select2['Time'].values, index=ghi_select2['SIS'])
        ghi_series = pd.Series(ghi_series.index.values, index=ghi_series, name='dni')
        ghi_series = ghi_series.tz_localize(pytz.utc)

        times = ghi_series.index
        # Generate clearsky data using the Ineichen model, which is the default
        # The get_clearsky method returns a _dataframe with values for GHI, DNI,
        # and DHI
        clearsky = site_location.get_clearsky(times)
        # Get solar azimuth and zenith to pass to the transposition function
        solar_position = site_location.get_solarposition(times=times)

        POA_irradiance = irradiance.get_total_irradiance(
            surface_tilt=0,
            surface_azimuth=0,
            dni=dni_series,
            ghi=ghi_series,
            dhi=ghi_series - dni_series * np.cos(np.radians(solar_position['zenith'])),
            # dhi=clearsky['dhi'],
            dni_extra=irradiance.get_extra_radiation(times),
            solar_zenith=solar_position['apparent_zenith'],
            solar_azimuth=solar_position['azimuth'],
            albedo=0.2,
            model="perez")
        POA_irradiance = POA_irradiance.resample("1M").mean()

        POA_irradiance.plot()
        plt.title(f'Irradiance for latitude: {round(latitude,3)} longitude: {round(longitude,3)}')
        plt.xlabel("Time")
        plt.ylabel("Irradiance in $w/m^2$")
        plt.tight_layout()
        plt.savefig(f'../../irradiance_graph_for_every_point/irradiance_{round(latitude,3)}_{round(longitude,3)}.png')
        plt.close()

        z_axis.append(POA_irradiance)
with open(file='zaxis.pickle', mode="wb") as file:
    pickle.dump(z_axis, file)

with open('coordinates.pickle', mode="wb") as file:
    pickle.dump(coordinates, file)

