from multiprocessing import Pool
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import tqdm
import os

dataframe = pd.read_pickle('../../out/refactored_satellite_data/RAYT_HORAIRE_DIRECT_2019.pickle')


def plot_graph(time: pd.Timestamp) -> None:
    """
    Plots SIS data for a specific time relative to the latitude and the longitude.
    :param time:
    """
    out = dataframe[dataframe.Time == time]
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.plot_trisurf(out.lat.to_list(), out.lon.to_list(), out.SID.to_list())
    ax.set_xlabel('latitude')
    ax.set_ylabel('longitude')
    ax.set_zlabel('SID')
    ax.set_zlim(0, 1200)
    fig.suptitle(str(time))
    fig.savefig(r'..\..\out\graphs\/RAYT_HORAIRE_DIRECT_2019\\' + str(time).replace(':', '_') + '.png')
    plt.close(fig)


if __name__ == '__main__':
    NUMBER_OF_POSSESS = 1
    times = pd.date_range('2019-01-01 00:00:00', '2019-12-31 23:30:00', freq='1M')  # 2019-11-01 23:30:00
    pool = Pool(NUMBER_OF_POSSESS)
    for result in tqdm.tqdm(pool.imap_unordered(plot_graph, times), total=len(times)):
        pass
    pool.close()
