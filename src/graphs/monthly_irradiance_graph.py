import matplotlib.pyplot as plt
import pickle
import pandas as pd
import numpy as np
import shapefile
import calendar
import os

sf = shapefile.Reader("../../shapefiles/mauritius_coastline.shp")

z_axis_filehandler = open("zaxis.pickle", "rb")
z_axis = pickle.load(z_axis_filehandler)
z_axis_filehandler.close()
lat_lon_coordinates_filehandler = open("coordinates.pickle", "rb")
lat_lon_coordinates = pickle.load(lat_lon_coordinates_filehandler)
minimum_irradiance = min([min(z_axis[month]['poa_global']) for month in range(0, 12)])
maximum_irradiance = max([max(z_axis[month]['poa_global']) for month in range(0, 12)])


def plot_map(sf, x_lim=None, y_lim=None):
    '''
    Plot map with lim coordinates
    '''
    id = 0
    for shape in sf.shapeRecords():
        x = [i[0] for i in shape.shape.points[:]]
        y = [i[1] for i in shape.shape.points[:]]
        plt.plot(x, y, 'k')

        if (x_lim is None) & (y_lim is None):
            x0 = np.mean(x)
            y0 = np.mean(y)
            # plt.text(x0, y0, id, fontsize=10)
        id += 1

    if (x_lim is not None) & (y_lim is not None):
        plt.xlim(x_lim)
        plt.ylim(y_lim)


for month in range(0, 12):
    fig, ax = plt.subplots()
    fig.set_dpi(1200)

    coordinates = []

    for i, data in enumerate(lat_lon_coordinates):
        lat, lon = data
        coordinates.append((lat, lon, z_axis[i].iloc(0)[month]['poa_global']))

    df = pd.DataFrame(coordinates, columns=list('XYZ'))
    ax.set_title(f"Mauritius global irradiance for {calendar.month_name[month + 1]}")
    ax.set_ylabel('latitude')
    ax.set_xlabel('longitude')

    # tcp = ax.tripcolor(df["Y"], df["X"], df["Z"], alpha=1, cmap='inferno', vmin=minimum_irradiance,
    #                    vmax=maximum_irradiance)
    contourf = ax.tricontourf(df["Y"], df["X"], df["Z"], )
    contour = ax.tricontour(df["Y"], df["X"], df["Z"], )

    ax.clabel(contour, inline=1, fontsize=7, colors='black')
    fig.colorbar(contourf, label='global irradiance in $W/m^2$')
    plot_map(sf)
    plt.xlim([57.3, 57.84])
    plt.ylim([-20.55, -19.95])

    plt.savefig('../../output_irradiance_graph/' + calendar.month_name[month + 1] + '_global_irradiance_graph.png')
    plt.close()
