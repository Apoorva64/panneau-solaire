import os
import pickle

import pandas as pd
import shapefile
import matplotlib.pyplot as plt
import numpy as np

sf = shapefile.Reader("../../shapefiles/mauritius_coastline.shp")

dataframe = pd.read_csv('../../out/best_tilt_azimuth_TF/optimized_tf.csv')
dataframe = dataframe.T
dataframe = dataframe.iloc[1:, :]

z_axis = list(dataframe[2])
lat_lon_coordinates_filehandler = open("coordinates.pickle", "rb")
lat_lon_coordinates = pickle.load(lat_lon_coordinates_filehandler)
minimum_transposition_factor = min(z_axis)
maximum_transposition_factor = max(z_axis)


def plot_map(sf, x_lim=None, y_lim=None):
    '''
    Plot map with lim coordinates
    '''
    id = 0
    for shape in sf.shapeRecords():
        x = [i[0] for i in shape.shape.points[:]]
        y = [i[1] for i in shape.shape.points[:]]
        plt.plot(x, y, 'k')

        if (x_lim is None) & (y_lim is None):
            x0 = np.mean(x)
            y0 = np.mean(y)
            # plt.text(x0, y0, id, fontsize=10)
        id += 1

    if (x_lim is not None) & (y_lim is not None):
        plt.xlim(x_lim)
        plt.ylim(y_lim)


fig, ax = plt.subplots()
fig.set_dpi(1200)

coordinates = []

for i, data in enumerate(lat_lon_coordinates):
    lat, lon = data
    coordinates.append((lat, lon, z_axis[i]))

df = pd.DataFrame(coordinates, columns=list('XYZ'))

ax.set_title(f"Transposition factor in Mauritius")
ax.set_ylabel('latitude')
ax.set_xlabel('longitude')

# ax.tripcolor(df["Y"], df["X"], df["Z"], alpha=1, cmap='YlOrRd', vmin=minimum_transposition_factor,
#                    vmax=maximum_transposition_factor)
contourf = ax.tricontourf(df["Y"], df["X"], df["Z"], cmap='YlOrRd', vmin=minimum_transposition_factor,
                          vmax=maximum_transposition_factor)
contour = ax.tricontour(df["Y"], df["X"], df["Z"], cmap='YlOrRd')

ax.clabel(contour, inline=1, fontsize=7, colors='black')
fig.colorbar(contourf, label='transposition factor')
plot_map(sf)

plt.xlim([57.3, 57.84])
plt.ylim([-20.55, -19.95])

plt.savefig('../../transposition_factor_graph.png')
plt.close()
