from multiprocessing import Pool
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import tqdm
import matplotlib.patches as mpatches
import os

from mpl_toolkits.mplot3d.art3d import Poly3DCollection

dataframe = pd.read_pickle('../../out/refactored_satellite_data/RAYT_HORAIRE_DIRECT_GLOBAL_2019.pickle')


def plot_graph(time: pd.Timestamp) -> None:
    """
    Plots SIS data for a specific time relative to the latitude and the longitude.
    :param time:
    """
    Poly3DCollection._edgecolors2d = ''
    Poly3DCollection._facecolors2d = ''
    out = dataframe[dataframe.Time == time]
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.plot_trisurf(out.lat.to_list(), out.lon.to_list(), out.SID.to_list(), color='blue' )
    ax.set_xlabel('latitude')
    ax.set_ylabel('longitude')
    ax.set_zlim(0, 1200)
    fig.suptitle(str(time))
    ax.plot_trisurf(out.lat.to_list(), out.lon.to_list(), out.SIS.to_list(), color='orange')
    ax.set_zlim(0, 1200)
    fig.suptitle(str(time))
    pop_a = mpatches.Patch(color='blue', label='SID')
    pop_b = mpatches.Patch(color='orange', label='SIS')

    plt.legend(handles=[pop_a, pop_b])
    # fig.show()
    fig.savefig(r'..\..\out\graphs\RAYT_HORAIRE_DIRECT_GLOBAL_2019\\' + str(time).replace(':', '_') + '.png')
    plt.close(fig)


if __name__ == '__main__':
    NUMBER_OF_POSSESS = 1
    times = pd.date_range('2019-01-01 00:00:00', '2019-12-31 23:30:00', freq='30min')  # 2019-11-01 23:30:00

    for time in tqdm.tqdm(times):
        plot_graph(time)

    # pool = Pool(NUMBER_OF_POSSESS)
    # for result in tqdm.tqdm(pool.imap_unordered(plot_graph, times), total=len(times)):
    #     pass
    # pool.close()
