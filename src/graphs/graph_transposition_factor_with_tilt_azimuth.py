import os
import pickle

import pandas as pd
import shapefile
import matplotlib.pyplot as plt
import numpy as np
import tqdm

sf = shapefile.Reader("../../shapefiles/mauritius_coastline.shp")

dataframe = pd.read_csv('../../out/best_tilt_azimuth_TF/optimized_tf.csv')
dataframe = dataframe.T
dataframe = dataframe.iloc[1:, :]

z_axis = list(dataframe[2])
lat_lon_coordinates_filehandler = open("coordinates.pickle", "rb")
lat_lon_coordinates = pickle.load(lat_lon_coordinates_filehandler)
minimum_transposition_factor = min(z_axis)
maximum_transposition_factor = max(z_axis)


def plot_map(sf, x_lim=None, y_lim=None):
    '''
    Plot map with lim coordinates
    '''
    id = 0
    for shape in sf.shapeRecords():
        x = [i[0] for i in shape.shape.points[:]]
        y = [i[1] for i in shape.shape.points[:]]
        plt.plot(x, y, 'k',
                 linewidth=0.7)

        if (x_lim is None) & (y_lim is None):
            x0 = np.mean(x)
            y0 = np.mean(y)
            # plt.text(x0, y0, id, fontsize=10)
        id += 1

    if (x_lim is not None) & (y_lim is not None):
        plt.xlim(x_lim)
        plt.ylim(y_lim)


fig, ax = plt.subplots()
fig.set_dpi(1200)

coordinates = []

for i, data in enumerate(lat_lon_coordinates):
    lat, lon = data
    coordinates.append((lat, lon, z_axis[i]))

df = pd.DataFrame(coordinates, columns=list('XYZ'))

ax.set_title(f"Cartography for Optimum Tilt & Azimuth angle \nfor PV solar panels in Mauritius", fontsize=8)
ax.set_ylabel('latitude', fontsize=5)
ax.set_xlabel('longitude', fontsize=5)
ax.set_zorder(3)

plt.grid(visible=True, zorder=10)

contourf = ax.tricontourf(df["Y"], df["X"], df["Z"], cmap='YlOrRd', vmin=minimum_transposition_factor,
                          vmax=maximum_transposition_factor)

latitude_major_ticks = np.round(np.arange(df["X"].min() - 0.025, df["X"].max() + 0.025, 0.05), 2)
longitude_major_ticks = np.round(np.arange(df["Y"].min() - 0.025, df["Y"].max() + 0.025, 0.05), 2)


plt.xticks(fontsize=5)
plt.yticks(fontsize=5)


ax.set_xticks(longitude_major_ticks)
ax.set_yticks(latitude_major_ticks)
# ax.clabel(contour, inline=1, fontsize=7, colors='black')
cb = fig.colorbar(contourf)

cb.set_label(label='transposition factor', size=5)
cb.ax.tick_params(labelsize=5)

plot_map(sf)
plt.xticks(rotation=45)

refactored_dataframe = pd.read_excel('../../transposition_factor_reformatted.xlsx')
refactored_dataframe_T = refactored_dataframe.T
for index in tqdm.tqdm(refactored_dataframe_T):
    plt.text(refactored_dataframe_T[index]['longitude'], refactored_dataframe_T[index]['latitude'],
             f"{refactored_dataframe_T[index]['optimal tilt']}\n"
             f"{refactored_dataframe_T[index]['optimal azimuth']}", fontsize=4, ha='center', va='center')

plt.xlim([df["Y"].min() - 0.025, df["Y"].max() + 0.025])
plt.ylim([df["X"].min() - 0.025, df["X"].max() + 0.025])
plt.tight_layout()

plt.savefig('../../transposition_factor_graph_with_tilt_azimuth.png')
plt.close()
