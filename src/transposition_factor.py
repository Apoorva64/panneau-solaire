import math

from pvlib import location, irradiance
import pandas as pd
import pytz
import numpy as np
import read_satellite_data

DNI = pd.read_pickle('../out/refactored_satellite_data/RAYT_HORAIRE_DIRECT_2019.pickle')
GHI = dataframe = pd.read_pickle('../out/refactored_satellite_data/RAYT_HORAIRE_GLOBAL_2019.pickle')
optimised_tilt_azimuth_df = pd.DataFrame()
latitude_coordinates = pd.unique(DNI['lat'])
longitude_coordinates = pd.unique(DNI['lon'])

n = 0
for latitude in latitude_coordinates:
    for longitude in longitude_coordinates:

        n += 1
        optimised_tilt_azimuth = (0, 0)
        max_transposition_factor = 0

        # For this example, we will be using Golden, Colorado
        tz = pytz.timezone("Etc/GMT+4")
        lat, lon = latitude, longitude
        #
        # # Create location object to store lat, lon, timezone
        site_location = location.Location(lat, lon, tz=tz)

        # convert the data in appropriate format
        dni_select1 = DNI[DNI['lat'] == latitude]
        dni_select2 = dni_select1[dni_select1['lon'] == longitude]
        dni_series = pd.Series(dni_select2['Time'].values, index=dni_select2['SID'])
        dni_series = pd.Series(dni_series.index.values, index=dni_series, name='dni')
        dni_series = dni_series.tz_localize(pytz.utc)

        ghi_select1 = GHI[GHI['lat'] == latitude]
        ghi_select2 = ghi_select1[ghi_select1['lon'] == longitude]
        ghi_series = pd.Series(ghi_select2['Time'].values, index=ghi_select2['SIS'])
        ghi_series = pd.Series(ghi_series.index.values, index=ghi_series, name='dni')
        ghi_series = ghi_series.tz_localize(pytz.utc)

        # Get the times from the dataset
        times = ghi_series.index
        # Generate clearsky data using the Ineichen model, which is the default
        # The get_clearsky method returns a _dataframe with values for GHI, DNI,
        # and DHI
        # clearsky = site_location.get_clearsky(times)
        # Get solar azimuth and zenith to pass to the transposition function
        solar_position = site_location.get_solarposition(times=times)


        def get_irradiance(_tilt: float, _surface_azimuth: float):
            """
            Use the get_total_irradiance function to transpose the GHI to POA

            :param _tilt: Tilt of the solar panel
            :param _surface_azimuth: surface azimuth of the solar panel
            :return: Plane of array global irradiance in W/m^2 over a year
            """

            poa_irradiance = irradiance.get_total_irradiance(
                surface_tilt=_tilt,
                surface_azimuth=_surface_azimuth,
                dni=dni_series,
                ghi=ghi_series,
                dhi=ghi_series - dni_series * np.cos(np.radians(solar_position['zenith'])),
                # dhi=clearsky['dhi'],
                dni_extra=irradiance.get_extra_radiation(times),
                solar_zenith=solar_position['apparent_zenith'],
                solar_azimuth=solar_position['azimuth'],
                albedo=0.2,
                model="perez")
            return poa_irradiance['poa_global']


        def get_transposition_factor(_tilt, _surface_azimuth):
            """

            :param _tilt: Tilt of the solar panel
            :param _surface_azimuth: surface azimuth of the solar panel
            :return: the transposition factor relative to the tilt and the azimuth of the panel
            """
            # return get_irradiance(_tilt, _surface_azimuth).mean() / get_irradiance(_tilt=0,
            #                                                                        _surface_azimuth=_surface_azimuth).mean()

            return get_irradiance(_tilt, _surface_azimuth).resample('1M').mean().sum() / get_irradiance(0,
                                                                                                        _surface_azimuth).resample(
                '1M').mean().sum()


        dataframe = pd.DataFrame()
        for tilt in range(0, 100, 10):
            column_name = f"Tilt-{tilt}"
            array = []
            array_index = []
            for surface_azimuth in range(-180, 190, 10):
                transposition_factor = get_transposition_factor(tilt, surface_azimuth)
                if transposition_factor > max_transposition_factor:
                    max_transposition_factor = transposition_factor
                    optimised_tilt_azimuth = (tilt, surface_azimuth)
                array.append(transposition_factor)
                array_index.append("surface azimuth " + str(surface_azimuth))
            dataframe[column_name] = pd.Series(array, array_index)

        optimised_tilt_azimuth_df[f'latitude{latitude},longitude{longitude}'] = pd.Series(
            [optimised_tilt_azimuth[0], optimised_tilt_azimuth[1], max_transposition_factor],
            ['_tilt', 'azimuth', 'Transposition factor'])

        print(f"data/longitude_{longitude}_latitude_{latitude}.csv {n} point")
        dataframe.to_csv(f"../out/TF_for_every_point/longitude_{longitude}_latitude_{latitude}.csv")
optimised_tilt_azimuth_df.to_csv(r'..\out\best_tilt_azimuth_TF\optimized_tf.csv')
